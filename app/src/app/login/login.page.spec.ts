import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LOGINPage } from './login.page';

describe('LOGINPage', () => {
  let component: LOGINPage;
  let fixture: ComponentFixture<LOGINPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LOGINPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LOGINPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
